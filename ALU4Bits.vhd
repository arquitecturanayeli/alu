
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity ALU4Bits is 
	generic (
		n : integer := 4
	);
	Port(
		A, B: in std_logic_vector (n-1 downto 0);
		op: in std_logic_vector (1 downto 0);
		a_sel, b_sel: in std_logic;		
		Cout: inout std_logic;
		s: inout std_logic_vector(n-1 downto 0);
		flag: out std_logic_vector(3 downto 0)
		--0:c;
		--1:n;
		--2:z;
		--3:ov;

	);
end ALU4Bits;


architecture Behavioral of ALU4Bits is
component alu 
    Port ( a,b: in  STD_LOGIC;	
			  a_sel,b_sel: in STD_LOGIC;
			  cin: in STD_LOGIC;
           op : in  STD_LOGIC_VECTOR (1 downto 0);
           s : out  STD_LOGIC;
			  Cout : out std_logic);
end component;
signal c : std_logic_vector (n downto 0);

begin
	c(0)<= b_sel;
	GEN_ALU:for i in 0 to n-1 generate      
		ALUs: alu port map(
			a => A(i),
			b => B(i),			
			a_sel => a_sel,
			b_sel => b_sel,
			Cin => c(i),
			op => op,
			s => s(i),
			Cout => c(i+1)
		);
   end generate GEN_ALU;
	Cout <= c(n);
	
	flag(0) <= Cout;
	flag(1) <= s(n-1);
	flag(2) <= Cout xor c(n-1);
	flag(3) <= '1' WHEN s = "0000" ELSE '0';
end Behavioral;

