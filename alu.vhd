
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity alu is
    Port ( a,b: in  STD_LOGIC;	
			  a_sel,b_sel: in STD_LOGIC;
			  cin: in STD_LOGIC;
           op : in  STD_LOGIC_VECTOR (1 downto 0);
           s : out  STD_LOGIC;
			  Cout : out std_logic);
end alu;

architecture Behavioral of alu is
signal Eb: std_logic;

signal a_aux: std_logic;
signal b_aux: std_logic;
begin
	process(a,b,a_sel,b_sel,op,a_aux,b_aux,cin,eb)
	begin
		a_aux<= a xor (a_sel);
		b_aux<= b xor (b_sel);
		case op is
			when "00" => s <= a_aux and b_aux;
			when "01" => s <= a_aux or b_aux;
			when "10" => s <= a_aux xor b_aux;
			when others =>
				s <= b_aux xor a_aux xor cin;
				Cout <= (b_aux and a_aux) or (a_aux and Cin) or (b_aux and Cin);
		end case;
	end process;
	

end Behavioral;