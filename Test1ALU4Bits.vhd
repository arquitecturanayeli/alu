LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Test1ALU4Bits IS
END Test1ALU4Bits;
 
ARCHITECTURE behavior OF Test1ALU4Bits IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ALU4Bits
    PORT(
         A : IN  std_logic_vector(3 downto 0);
         B : IN  std_logic_vector(3 downto 0);
         op : IN  std_logic_vector(1 downto 0);
         a_sel : IN  std_logic;
         b_sel : IN  std_logic;
         Cout : INOUT  std_logic;
         s : INOUT  std_logic_vector(3 downto 0);
         flag : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(3 downto 0) := (others => '0');
   signal B : std_logic_vector(3 downto 0) := (others => '0');
   signal op : std_logic_vector(1 downto 0) := (others => '0');
   signal a_sel : std_logic := '0';
   signal b_sel : std_logic := '0';

	--BiDirs
   signal Cout : std_logic;
   signal s : std_logic_vector(3 downto 0);

 	--Outputs
   signal flag : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ALU4Bits PORT MAP (
          A => A,
          B => B,
          op => op,
          a_sel => a_sel,
          b_sel => b_sel,
          Cout => Cout,
          s => s,
          flag => flag
        );


   -- Stimulus process
   stim_proc: process
   begin		
     op<="00";
	  A<="0101";
	  B<="0110";
	  a_sel<='0';
	  b_sel<='0';
	  
	  wait for 100 ns;
	  
	  op<="01";
	  A<="0101";
	  B<="0110";
	  a_sel<='0';
	  b_sel<='0';
	  
	  wait for 100 ns;
	  
	  op<="10";
	  A<="0101";
	  B<="0110";
	  a_sel<='0';
	  b_sel<='0';
	  
	  wait for 100 ns;
	  
	  op<="11";
	  A<="0101";
	  B<="0110";
	  a_sel<='0';
	  b_sel<='0';
	  
	  wait for 100 ns;

      wait;
   end process;

END;
